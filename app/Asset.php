<?php
namespace App;

class Asset
{

    public function path($filename)
    {
        $parts = explode('.', $filename);
        $json = json_decode(file_get_contents(public_path() . '/assets/assets.json'), true);
        if (self::isLocal()) {
            return 'http://localhost/assets/' . $filename;
        } else {
            return $json[$parts[0]][$parts[1]];
        }
    }

    public static function isLocal() {
        return strpos($_SERVER['HTTP_HOST'], 'localhost') !== false;
    }

}
