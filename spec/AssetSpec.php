<?php

use App\Asset;
use Kahlan\Plugin\Monkey;
use Kahlan\Plugin\Stub;

describe('Asset::class', function () {

    describe('->path', function () {

        given('json', function () {
           return '{"app":{"js":"/assets/app.920fc8a1.js","css":"/assets/app.f5555616.css"}}';
        });

        given('asset', function () {
            return new Asset();
        });

        beforeEach(function () {
            Monkey::patch('public_path', function () { return ''; });
            Monkey::patch('file_get_contents', function ()  { return $this->json; });

        });

        it('resolves the correct path', function () {
            Stub::on(Asset::class)->method('::isLocal')->andReturn(false);

            expect($this->asset->path('app.js'))->toBe('/assets/app.920fc8a1.js');
        });

        it('resolves the correct path if we are on localhost', function () {
            Stub::on(Asset::class)->method('::isLocal')->andReturn(true);
            expect($this->asset->path('app.js'))->toBe('http://localhost/assets/app.js');
        });
    });
});
